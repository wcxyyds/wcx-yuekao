import Link from 'next/link'
export default () => <div className="allquanbu">
    <div className="all_header">
        <div className="all_header_img">
            <img src='https://www.lufaxholding.com/zh-cn/index' alt="lufax" />
            <div className="all_header_zi">
                <Link href="/home" style={{ color: 'red' }}>
                    <a>首页</a>
                </Link>{''}
                <Link href='/about'>
                    <a> 关于我们</a>
                </Link>{''}
                <Link href="/bas">
                    <a> 业务与服务</a>
                </Link>
                <Link href="/pressCenter">
                    <a>新闻中心</a>
                </Link>
                <Link href="/investor">
                    <a>投资者</a>
                </Link>
                <Link href="/connecrus">
                    <a>联系我们</a>
                </Link>
                <Link href="/joinus">
                    <a>加入我们</a>
                </Link>
                <Link href="/en">
                    <a>简/EN</a>
                </Link>
            </div>
        </div>
        <div className="imgs_pig">
            <h1>
                我们的使命
            </h1>
            <h3>
                让零售信贷和财富管理更简单，更安全
            </h3>
        </div>
        <div className="all_Main">
            <div className="all_main_zuo">
                <h1>  精准自能的</h1>
                <h1> 财富管理服务</h1>
                <span> 基本深度学习和大数据分析应用，平台支持自动化组合投资工具，制定定制化且匹配投资者风险偏好</span>

                <div className="all_main_zui_xia">
                    了解更多→
                </div>
            </div>
            <div className="all_mina_img">
                <img src="/" />
            </div>
        </div>
        <div className="all_Main">
        <div className="all_mina_img">
                <img src="/" />
            </div>
            <div className="all_main_you">
                <h1>  便捷可达的</h1>
                <h1> 零售信贷服务</h1>
                <span> 拥有多向先进科技应用，权限上业务流程为客户提供信贷废物，瞒住小微企业住信贷需求</span>

                <div className="all_main_you_xia">
                    了解更多→
                </div>
            </div>
           
        </div>
    </div>
    <div className="all_main-footer">
            <div className="all_main_footer_zuo">
                <div>
                    <h1>
                        <Link href="">
                            <a> 地址:shanghai</a>
                            </Link>
                    </h1>
                </div>
            </div>
    </div>
</div>